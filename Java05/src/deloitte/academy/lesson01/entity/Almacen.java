package deloitte.academy.lesson01.entity;

import java.util.ArrayList;

import deloitte.academy.lesson01.abs.Proveedor;
import eloitte.academy.lesson01.logic.Logic;

/**
 * Esta clase permite la reaci�n de los almacenes con sus respectivas
 * propiedades, como su nombre, inventario, y ventas.
 * 
 * @author rubemendoza
 *
 */
public class Almacen {
	private int id;
	private String nombre;
	private ArrayList<RegistroProducto> inventario = new ArrayList<RegistroProducto>();
	private ArrayList<Venta> ventas = new ArrayList<Venta>();

	public Almacen() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Almacen(int id, String nombre) {
		super();
		this.id = id;
		this.nombre = nombre;
		// Agregar almacen a la lista de almacenes registrados
		Logic.almacenes.add(this);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public ArrayList<RegistroProducto> getInventario() {
		return inventario;
	}

	public void setInventario(ArrayList<RegistroProducto> inventario) {
		this.inventario = inventario;
	}

	/**
	 * venderProducto es un m�todo que sirve para poder vender un producto
	 * seleccionado, causando que el inventario disminuya la cantidad de elementos
	 * que tiene en stock, en caso de no tener el producto, se notificar� al
	 * usuario.
	 * 
	 * @param idProducto      el identificador del producto que se quiere vender
	 * @param cantidadVendida el n�mero de elementos que se desean vender
	 */
	public void venderProducto(int idProducto, int cantidadVendida) {
		// Verificar si existe el producto
		int index = 0;

		for (RegistroProducto rp : this.inventario) {
			// se verifica que exista el producto en el mercado
			if (rp.getIdProducto() == idProducto) {
				int currentQty = this.inventario.get(index).getCantidad();

				// se hace una validaci�n para evitar vender m�s de lo que se tiene
				if (cantidadVendida > currentQty) {
					this.inventario.get(index).setCantidad(0);
				} else {
					this.inventario.get(index).setCantidad(currentQty - cantidadVendida);
				}

				currentQty = this.inventario.get(index).getCantidad();

				// se almacena la venta realizada
				for (Producto p : Logic.productos) {
					if (p.getId() == idProducto) {
						Venta venta = new Venta(p.getPrecio(), p.getNombre(), cantidadVendida);
						this.ventas.add(venta);
						System.out.println(p.getNombre() + " vendido(s) con �xito.");
					}
				}

				return;
			}

			index++;
		}

		System.out.println("No se puede vender el producto porque no existe en el inventario.");
	}

	/**
	 * Este m�todo permite al usuario solicitar productos espec�ficos al proveedor
	 * as� como la cantidad deseada
	 * 
	 * @param idProducto  es el identificador del producto que se quiere pedir a al
	 *                    proveedor
	 * @param idProveedor identificador del proveedor al que se le solicitan los
	 *                    productos
	 * @param cantidad    el n�mero de elementos que se le solicitan al proveedor
	 */
	public void solicitarProducto(int idProducto, int idProveedor, int cantidad) {
		for (Proveedor proveedor : Logic.proveedores) {
			// se verifica que exista el proveedor
			if (proveedor.getId() == idProveedor) {
				for (Producto producto : Logic.productos) {
					// se verifica que exista el producto en el stock de origen
					if (producto.getId() == idProducto) {
						for (RegistroProducto rp : this.inventario) {
							// se hace una validaci�n para actualizar el stock de origen
							if (rp.getIdProducto() == idProducto) {
								rp.setCantidad(rp.getCantidad() + cantidad);
								System.out.println(
										producto.getNombre() + " solicitado(s) al proveedor " + proveedor.getNombre());
								return;
							}
						}

						this.inventario.add(new RegistroProducto(producto.getId(), cantidad, producto.getNombre()));
						System.out.println(cantidad + " " + producto.getNombre() + " solicitado(s) al proveedor "
								+ proveedor.getNombre());
						return;
					}
				}

				System.out.println("Ese producto no existe en el mercado.");
				return;
			}
		}

		System.out.println("Ese proveedor no existe.");
	}

	/**
	 * Este m�todo permite mostrar el inventario de un almac�n espec�fico
	 */
	public void mostrarInventario() {
		System.out.println("\n:: MOSTRANDO INVENTARIO EN " + this.nombre.toUpperCase());
		for (RegistroProducto r : this.inventario) {
			System.out.println(r.getCantidad() + " " + r.getNombre());
		}
		System.out.println();
	}

	/**
	 * Este m�todo permite al usuario mover inventario de un almac�n a otro
	 * 
	 * @param destino    es el id del almacen de destino
	 * @param idProducto es l identificador del producto que se quiere transferir
	 * @param cantidad   la cantidad de elementos que se quieren mover de un almac�n
	 *                   a otro
	 */
	public void traspasoAlmacen(Almacen destino, int idProducto, int cantidad) {
		for (Almacen almacen : Logic.almacenes) {
			// se verifica que exista el almac�n de destino
			if (destino.getId() == almacen.getId()) {
				System.out.println(
						":: TRANSFIRIENDO DE " + this.nombre.toUpperCase() + " A " + destino.getNombre().toUpperCase());
				int indexProducto = 0;
				for (RegistroProducto rp : this.inventario) {
					// se verifica que el producto exista en el stock de origen
					if (idProducto == rp.getIdProducto()) {
						// se valida que la cantidad no sobrepase al stock
						if (rp.getCantidad() < cantidad) {
							System.out.println("No fue posible transferir esa cantidad de " + rp.getNombre() + "(s)\n");
							return;
						}

						// se cambia la cantidad de elementos en el stock de origen
						int currentQtyOrigen = this.inventario.get(indexProducto).getCantidad();
						this.inventario.get(indexProducto).setCantidad(currentQtyOrigen - cantidad);

						// se cambia la cantidad de elementos en el stock de destinno
						for (int i = 0; i < destino.getInventario().size(); i++) {
							if (destino.getInventario().get(i).getIdProducto() == idProducto) {
								int currentQtyDestino = destino.getInventario().get(i).getCantidad();
								destino.getInventario().get(i).setCantidad(cantidad + currentQtyDestino);
								return;
							}
						}

						destino.getInventario().add(new RegistroProducto(idProducto, cantidad, rp.getNombre()));
						return;
					}

					indexProducto++;
				}

				System.out.println("No existe el producto que se quiere transferir");
			}
		}

		System.out.println("No existe el almacen indicado.");
	}

	/**
	 * Este m�todo muestra las ventas realizadas en un almac�n en espec�fico, no
	 * requiere de argumentos
	 */
	public void mostrarVentas() {
		System.out.println(":: MOSTRANDO VENTAS EN " + this.nombre.toUpperCase());

		double total = 0;

		for (Venta venta : this.ventas) {
			System.out.println("Venta de " + venta.getCantidad() + " " + venta.getNombreProducto().toUpperCase()
					+ " por $" + venta.getTransaccion() * venta.getCantidad());
			total += venta.getTransaccion() * venta.getCantidad();
		}

		System.out.println("Monto total: $" + total + "\n");
	}
}
