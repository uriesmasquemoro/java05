package deloitte.academy.lesson01.entity;

import deloitte.academy.lesson01.inter.Crud;
import eloitte.academy.lesson01.logic.Logic;

public class Producto implements Crud {
	private int id;
	private double precio;
	private String nombre;
	private String categoria;

	public Producto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Producto(int id, double precio, String nombre, String categoria) {
		super();
		this.id = id;
		this.precio = precio;
		this.nombre = nombre;
		this.categoria = categoria;
		Logic.productos.add(this);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	@Override
	public void create() {
		// TODO Auto-generated method stub

	}

	@Override
	public void read() {
		// TODO Auto-generated method stub

	}

	@Override
	public void update() {
		// TODO Auto-generated method stub

	}

	@Override
	public void delete() {
		// TODO Auto-generated method stub
		int index = 0;

		for (Producto p : Logic.productos) {
			if (this.getNombre().contentEquals(p.getNombre())) {
				break;
			}
			
			index++;
		}

		System.out.println(index);
		Logic.productos.remove(index);
	}

}
