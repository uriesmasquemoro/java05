package deloitte.academy.lesson01.entity;

/**
 * Esta clase permite el almacenamiento de las ventas que se han realizado en un
 * determinado almacen
 * 
 * @author rubemendoza
 *
 */
public class Venta {
	private int cantidad;
	private double transaccion;
	private String nombreProducto;

	public Venta(double transaccion, String nombreProducto, int cantidad) {
		super();
		this.transaccion = transaccion;
		this.nombreProducto = nombreProducto;
		this.cantidad = cantidad;
	}

	public double getTransaccion() {
		return transaccion;
	}

	public void setTransaccion(double transaccion) {
		this.transaccion = transaccion;
	}

	public String getNombreProducto() {
		return nombreProducto;
	}

	public void setNombreProducto(String nombreProducto) {
		this.nombreProducto = nombreProducto;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}
	
	

}
