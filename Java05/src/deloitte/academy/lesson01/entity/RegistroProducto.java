package deloitte.academy.lesson01.entity;

public class RegistroProducto {
	private int idProducto;
	private int cantidad;
	private String nombre;

	public RegistroProducto(int idProducto, int cantidad, String nombre) {
		super();
		this.idProducto = idProducto;
		this.cantidad = cantidad;
		this.nombre = nombre;
	}

	public int getIdProducto() {
		return idProducto;
	}

	public void setIdProducto(int idProducto) {
		this.idProducto = idProducto;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	
}
