package deloitte.academy.lesson01.entity;

public enum Categorias {
	CAT1("Celulares"),
	CAT2("Aud�fonos");
	
	private String nombre;
	
	Categorias(String nombre) {
		this.nombre = nombre;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
}
