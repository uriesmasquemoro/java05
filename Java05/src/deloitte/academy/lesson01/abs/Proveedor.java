package deloitte.academy.lesson01.abs;

import eloitte.academy.lesson01.logic.Logic;

public abstract class Proveedor {
	private int id;
	private String nombre;

	public Proveedor(int id, String nombre) {
		super();
		this.id = id;
		this.nombre = nombre;
		// Agregar proveedor a la lista de proveedores registrados
		Logic.proveedores.add(this);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public abstract void surtirProducto(int idProducto, int idAlmacen);
}
