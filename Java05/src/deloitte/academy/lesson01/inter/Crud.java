package deloitte.academy.lesson01.inter;

public interface Crud {
	public void create();
	public void read();
	public void update();
	public void delete();
}
