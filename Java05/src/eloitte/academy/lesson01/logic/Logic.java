package eloitte.academy.lesson01.logic;

import java.util.ArrayList;

import deloitte.academy.lesson01.abs.Proveedor;
import deloitte.academy.lesson01.entity.Almacen;
import deloitte.academy.lesson01.entity.Categorias;
import deloitte.academy.lesson01.entity.Producto;
import deloitte.academy.lesson01.entity.ProveedorInternacional;
import deloitte.academy.lesson01.entity.ProveedorNacional;

public class Logic {
	public static ArrayList<Proveedor> proveedores = new ArrayList<Proveedor>();
	public static ArrayList<Almacen> almacenes = new ArrayList<Almacen>();
	public static ArrayList<Producto> productos = new ArrayList<Producto>();

	public static void ejecutar() {
		// Creaci�n de productos existentes en el mercado
		Producto mi9 = new Producto(1087, 6345.60, "Mi9", Categorias.CAT1.getNombre());
		Producto s10 = new Producto(2369, 16942.5, "S10", Categorias.CAT1.getNombre());
		Producto ubs = new Producto(8464, 1695.64, "Ultra Bass", Categorias.CAT2.getNombre());
		Producto dre = new Producto(4578, 7809.77, "Dre YW4", Categorias.CAT2.getNombre());

		// Creaci�n de proveedores (internacional y nacional)
		ProveedorInternacional lenovo = new ProveedorInternacional(100, "Lenovo");
		ProveedorNacional eagle = new ProveedorNacional(200, "Eagle Warrior");

		// Creaci�n de almacenes con pedidos a los proveedores
		Almacen queretaro = new Almacen(001, "Quer�taro");
		queretaro.venderProducto(1087, 10);
		queretaro.solicitarProducto(1087, 100, 30);
		queretaro.solicitarProducto(2369, 200, 30);
		queretaro.venderProducto(1087, 5);
		queretaro.venderProducto(2369, 2);
		queretaro.mostrarInventario();

		dre.delete(); // eliminar un producto para despu�s solicitarlo y ver qu� pasa
		Almacen monterrey = new Almacen(002, "Monterrey");
		monterrey.solicitarProducto(8464, 200, 50);
		monterrey.solicitarProducto(4578, 200, 34);
		monterrey.venderProducto(8464, 7);
		monterrey.mostrarInventario();

		queretaro.traspasoAlmacen(monterrey, 1087, 5);
		monterrey.mostrarInventario();
		queretaro.mostrarInventario();
		
		queretaro.mostrarVentas();
		monterrey.mostrarVentas();
	}
}
